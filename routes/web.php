<?php
use zcrmsdk\crm\setup\restclient\ZCRMRestClient;
use zcrmsdk\crm\crud\ZCRMCustomView;
use zcrmsdk\crm\crud\ZCRMRecord;
use zcrmsdk\crm\exception\ZCRMException;
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('app/public/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Auth::routes();

Route::get('/', ['uses' => 'HomeController@index', 'as' => 'app.home.index']);
Route::get('/about-us', ['uses' => 'HomeController@about', 'as' => 'app.home.about']);
Route::get('/contact-us', ['uses' => 'HomeController@contact', 'as' => 'app.home.contact']);
Route::get('/terms', ['uses' => 'HomeController@terms', 'as' => 'app.home.term']);
Route::get('/privacy', ['uses' => 'HomeController@privacy', 'as' => 'app.home.privacy']);

Route::get('/callback', function(){
    $configuration = array("client_id"=>"1000.3QS1SQ0HC68N067758TLG97ECN9N1H",
    "client_secret"=>"5c1d0eaff7b057a591d66506ee3126925a3e15103b",
    "redirect_uri"=>"https://sdc-system.app/",
    "currentUserEmail"=>"khaled@sdccards.com"); 
    

    $client = new ZohoCRMClient('Leads', 'b49f3c7aebceccba8cc3bddd087b34da');

    $records = $client->getRecords()
        ->selectColumns('First Name', 'Last Name', 'Email')
        ->sortBy('Last Name')->sortAsc()
        ->since(date_create('last week'))
        ->request();

    // Just for debug
    echo "<pre>";
    print_r($records);
    echo "</pre>";

});

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/user', 'UserController');
});
