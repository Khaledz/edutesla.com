<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_code_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('activation_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('date_from')->nullable();
            $table->string('date_to')->nullable();
            $table->integer('days_number')->nullable();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('type_id')->unsigned();
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('type_id')->references('id')->on('activation_code_types');
        });

        Schema::create('user_activation_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('activation_code_id')->unsigned();
            $table->string('expire_on');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('activation_code_id')->references('id')->on('activation_code_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activation_codes');
    }
}
