<?php

use Illuminate\Database\Seeder;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            'name' => 'Admin',
            'guard_name' => 'web',
        ]);

        \DB::table('roles')->insert([
            'name' => 'Operation',
            'guard_name' => 'web',
        ]);

        \DB::table('roles')->insert([
            'name' => 'Acountant',
            'guard_name' => 'web',
        ]);

        \DB::table('roles')->insert([
            'name' => 'Marketing',
            'guard_name' => 'web',
        ]);

        \DB::table('roles')->insert([
            'name' => 'Member',
            'guard_name' => 'web',
        ]);

        \DB::table('countries')->insert([
            'name' => 'المملكة العربية السعودية',
        ]);

        \DB::table('states')->insert([
            'name' => 'المنطقة الغربية',
            'country_id' => 1
        ]);

        \DB::table('cities')->insert([
            'name' => 'جدة',
            'state_id' => 1
        ]);

        \DB::table('users')->insert([
            'fname' => 'Khaled',
            'lname' => 'Bawazir',
            'email' => 'admin@sdc.com',
            'password' => \Hash::make('123456'),
            'phone' => 12345676,
            'city_id' => 1
        ]);

        \App\User::first()->assignRole('Admin');
        
        \DB::table('categories')->insert([
            'name' => 'المطاعم',
        ]);

        \DB::table('categories')->insert([
            'name' => 'مطاعم راقية',
            'parent_id' => 1
        ]);

        \DB::table('activation_code_types')->insert([
            'name' => 'تاريخ صلاحية',
        ]);

        \DB::table('activation_code_types')->insert([
            'name' => 'عدد اﻷيام',
        ]);

        \DB::table('products')->insert([
            'name' => 'إشتراك ستة شهور',
            'price' => 99,
            'vat' => 5,
        ]);

        \DB::table('products')->insert([
            'name' => 'إشتراك سنة واحدة',
            'price' => 129,
            'vat' => 5,
        ]);

        \DB::table('products')->insert([
            'name' => 'إشتراك سنتين',
            'price' => 199,
            'vat' => 5,
        ]);

        \DB::table('activation_codes')->insert([
            'code' => 'abc123',
            'date_from' => now(),
            'date_to' => now()->addDays(10),
            'product_id' => 2,
            'type_id' => 1
        ]);
        
        \DB::table('coupon_types')->insert([
            'name' => 'خصم بالنسبة المئوية',
        ]);

        \DB::table('coupon_types')->insert([
            'name' => 'خصم مبلغ ثابت',
        ]);

        \DB::table('coupon_types')->insert([
            'name' => 'خصم طويل اﻷمد',
        ]);

        \DB::table('app_settings')->insert([
            'name' => "SDC",
            'description' => "SDC Cards",
        ]);
        
        \DB::table('payment_methods')->insert([
            'name' => 'الدفع عند اﻹستلام',
        ]);
        
        \DB::table('payment_methods')->insert([
            'name' => 'الدفع أونلاين',
        ]);
    }
}
