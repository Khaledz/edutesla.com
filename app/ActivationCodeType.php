<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivationCodeType extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->hasMany('User');
    }
}
