<?php

if(! function_exists('twelveFormat'))
{
    function twelveFormat($time)
    {
        return \Carbon\Carbon::parse($time)->format('h:ia');
    }
}

if(! function_exists('dateFormat'))
{
    function dateFormat($date)
    {
        return \Carbon\Carbon::parse($date)->format('Y-m-d');
    }
}
