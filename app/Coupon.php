<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $guarded = [];

    public function type()
    {
        return $this->belongsTo('CouponType');
    }
}
