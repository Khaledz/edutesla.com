<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo('Menu', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('Menu');
    }
}
