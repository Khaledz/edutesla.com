<?php

namespace App\Observers;

use App\Slider;
use Cache;
use Illuminate\Http\Request;

class SliderObserver
{
    /**
     * Handle the sliders "created" event.
     *
     * @param  \App\Slider  $sliders
     * @return void
     */
    public function created(Slider $slider)
    {
        Cache::forget('sliders');        
    }

    /**
     * Handle the sliders "updated" event.
     *
     * @param  \App\Slider  $sliders
     * @return void
     */
    public function updated(Slider $sliders)
    {
        Cache::forget('sliders');
    }

    /*Slider
     *Slidersetting "deleted" event.
     *Slider
     * @param  \App\Slider  $sliders
     * @return void
     */
    public function deleted(Slider $sliders)
    {
        Cache::forget('sliders');
    }

    /**
     * Handle the sliders "restored" event.
     *
     * @param  \App\Slider  $sliders
     * @return void
     */
    public function restored(Slider $sliders)
    {
        //
    }

    /**
     * Handle the sliders "force deleted" event.
     *
     * @param  \App\Slider  $sliders
     * @return void
     */
    public function forceDeleted(Slider $sliders)
    {
        //
    }
}
