<?php

namespace App\Observers;

use App\Exercise;
use Cache;
use Illuminate\Http\Request;

class ExerciseObserver
{
    /**
     * Handle the exercises "created" event.
     *
     * @param  \App\Exercise  $exercises
     * @return void
     */
    public function created(Exercise $exercises)
    {
        Cache::forget('exercises');
    }

    /**
     * Handle the exercises "updated" event.
     *
     * @param  \App\Exercise  $exercises
     * @return void
     */
    public function updated(Exercise $exercises)
    {
        Cache::forget('exercises');
    }

    /*Exercise
     *Exercisesetting "deleted" event.
     *Exercise
     * @param  \App\Exercise  $exercises
     * @return void
     */
    public function deleted(Exercise $exercises)
    {
        Cache::forget('exercises');
    }

    /**
     * Handle the exercises "restored" event.
     *
     * @param  \App\Exercise  $exercises
     * @return void
     */
    public function restored(Exercise $exercises)
    {
        //
    }

    /**
     * Handle the exercises "force deleted" event.
     *
     * @param  \App\Exercise  $exercises
     * @return void
     */
    public function forceDeleted(Exercise $exercises)
    {
        //
    }
}
