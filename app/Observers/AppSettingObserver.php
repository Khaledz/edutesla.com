<?php

namespace App\Observers;

use App\AppSetting;
use Cache;

class AppSettingObserver
{
    /**
     * Handle the app_settings "created" event.
     *
     * @param  \App\AppSetting  $app_settings
     * @return void
     */
    public function created(AppSetting $app_settings)
    {
        Cache::forget('app_settings');
    }

    /**
     * Handle the app_settings "updated" event.
     *
     * @param  \App\AppSetting  $app_settings
     * @return void
     */
    public function updated(AppSetting $app_settings)
    {
        Cache::forget('app_settings');
    }

    /*AppSetting
     *AppSettingsetting "deleted" event.
     *AppSetting
     * @param  \App\AppSetting  $app_settings
     * @return void
     */
    public function deleted(AppSetting $app_settings)
    {
        Cache::forget('app_settings');
    }

    /**
     * Handle the app_settings "restored" event.
     *
     * @param  \App\AppSetting  $app_settings
     * @return void
     */
    public function restored(AppSetting $app_settings)
    {
        //
    }

    /**
     * Handle the app_settings "force deleted" event.
     *
     * @param  \App\AppSetting  $app_settings
     * @return void
     */
    public function forceDeleted(AppSetting $app_settings)
    {
        //
    }
}
