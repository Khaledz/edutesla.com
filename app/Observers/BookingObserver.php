<?php

namespace App\Observers;

use App\Booking;
use Cache;
use Illuminate\Http\Request;

class BookingObserver
{
    /**
     * Handle the bookings "created" event.
     *
     * @param  \App\Booking  $bookings
     * @return void
     */
    public function created(Booking $booking)
    {
        // take out the exercise price from user balance.
        auth()->user()->balance -= $booking->timetable->exercise->price;
        auth()->user()->save();
    }

    /**
     * Handle the bookings "updated" event.
     *
     * @param  \App\Booking  $bookings
     * @return void
     */
    public function updated(Booking $bookings)
    {
        // Cache::forget('bookings');
    }

    /*Booking
     *Bookingsetting "deleted" event.
     *Booking
     * @param  \App\Booking  $bookings
     * @return void
     */
    public function deleted(Booking $bookings)
    {
        // Cache::forget('bookings');
    }

    /**
     * Handle the bookings "restored" event.
     *
     * @param  \App\Booking  $bookings
     * @return void
     */
    public function restored(Booking $bookings)
    {
        //
    }

    /**
     * Handle the bookings "force deleted" event.
     *
     * @param  \App\Booking  $bookings
     * @return void
     */
    public function forceDeleted(Booking $bookings)
    {
        //
    }
}
