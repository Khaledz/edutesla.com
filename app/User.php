<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Cache;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'email', 'password', 'phone', 'balance', 'member_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function timetables()
    {
        return $this->belongsToMany('App\Timetable', 'users_timetables', 'user_id', 'timetable_id');
    }

    public function exercises()
    {
        return $this->belongsToMany('App\Exercise', 'users_exercises', 'user_id', 'exercise_id');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function getFullNameAttribute()
    {
        return $this->fname .' '. $this->lname;
    }

    public function isConfirmedBooked(Timetable $timetable)
    {
        $record = $this->bookings()->where('timetable_id', $timetable->id)->first();

        if(! is_null($record) && $record->is_confirmed == 1)
        {
            return true;
        }

        return false;
    }

    public function isPendingBooked(Timetable $timetable)
    {
        $record = $this->bookings()->where('timetable_id', $timetable->id)->first();
        if(! is_null($record) && $record->is_confirmed == 0)
        {
            return true;
        }

        return false;
    }

    public function isRegistered(Exercise $exercise)
    {
        $record = $this->exercises()->where('exercise_id', $exercise->id)->where('user_id', $this->id)->first();
        if(! is_null($record))
        {
            return true;
        }

        return false;
    }

    public function hasBooked(Timetable $timetable)
    {
        if($this->bookings()
            ->whereDate('date', '>=', now())
            ->where('timetable_id', $timetable->id)
            ->first())
        {
            return true;
        }

        return false;
    }

    public function getExercisePercentage()
    {
        $exercise_count = Cache::get('exercises')->count();
        $user_exercise_count = $this->exercises()->count();
        
        if($user_exercise_count == 0 || $user_exercise_count == 0)
        {
            return 0;
        }

        return intval($user_exercise_count / $exercise_count * 100);
    }

    // من اصل جميع الحجوزات
    public function getConfimredBookingPercentage()
    {
        $user_booking_count = $this->bookings()->count();
        $user_confirmed_booking_count = $this->bookings()->where('is_confirmed', true)->count();
        
        if($user_booking_count == 0)
        {
            return 0;
        }
        
        return intval($user_confirmed_booking_count / $user_booking_count * 100);
    }

    public function getCancellationPercentage()
    {
        return 0;
    }
}