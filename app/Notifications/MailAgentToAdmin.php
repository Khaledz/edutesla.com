<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Agent;

class MailAgentToAdmin extends Notification
{
    use Queueable;

    public $agent;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $agent)
    {
        $this->agent = $agent;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('New agent from the website.')
                    ->line('Company: ' . $this->agent['company'])
                    ->line('Person Name: ' . $this->agent['person_name'])
                    ->line('Email: ' . $this->agent['email'])
                    ->line('Phone: ' . $this->agent['phone']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
