<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponType extends Model
{
    protected $guarded = [];

    public function coupons()
    {
        return $this->hasMany('Coupon');
    }
}
