<?php

namespace App\Listeners;

use App\Event\BookingWasApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBookingWasApprovedToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingWasApproved  $event
     * @return void
     */
    public function handle(BookingWasApproved $event)
    {
        //
    }
}
