<?php

namespace App\Listeners;

use App\Providers\App\Events\IncreaseBalanceWasApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendIncreaseBalanceWasApprovedToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IncreaseBalanceWasApproved  $event
     * @return void
     */
    public function handle(IncreaseBalanceWasApproved $event)
    {
        //
    }
}
