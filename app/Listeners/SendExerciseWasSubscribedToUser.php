<?php

namespace App\Listeners;

use App\Events\ExerciseWasSubscribed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendExerciseWasSubscribedToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExerciseWasSubscribed  $event
     * @return void
     */
    public function handle(ExerciseWasSubscribed $event)
    {
        //
    }
}
