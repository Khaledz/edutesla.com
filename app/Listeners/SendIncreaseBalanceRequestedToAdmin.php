<?php

namespace App\Listeners;

use App\Events\IncreaseBalanceRequested;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendIncreaseBalanceRequestedToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IncreaseBalanceRequested  $event
     * @return void
     */
    public function handle(IncreaseBalanceRequested $event)
    {
        //
    }
}
