<?php

namespace App\Listeners;

use App\Events\ExerciseWasUnsubscribed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendExerciseWasUnsubscribedToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExerciseWasUnsubscribed  $event
     * @return void
     */
    public function handle(ExerciseWasUnsubscribed $event)
    {
        //
    }
}
