<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivationCode extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function type()
    {
        return $this->belongsTo('ActivationCodeType');
    }
}
