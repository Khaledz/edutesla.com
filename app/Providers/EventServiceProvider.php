<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\Registered::class => [
            \App\Listeners\SendEmailVerificationNotification::class,
        ],
        \App\Events\ExerciseWasSubscribed::class => [
            \App\Listeners\SendExerciseWasSubscribedToAdmin::class,
            \App\Listeners\SendExerciseWasSubscribedToUser::class
        ],
        \App\Events\ExerciseWasUnsubscribed::class => [
            \App\Listeners\RemoveAllBookingForExerciseWasUnsubscribed::class,
            \App\Listeners\SendExerciseWasUnsubscribedToAdmin::class,
            \App\Listeners\SendExerciseWasUnsubscribedToUser::class
        ],
        \App\Events\IncreaseBalanceRequested::class => [
            \App\Listeners\SendIncreaseBalanceRequestedToAdmin::class,
        ],
        App\Events\IncreaseBalanceWasApproved::class => [
            \App\Listeners\SendIncreaseBalanceWasApprovedToAdmin::class,
            \App\Listeners\SendIncreaseBalanceWasApprovedToUser::class,
        ],
        \App\Event\NewBooking::class => [
            \App\Listeners\SendIncreaseBalanceWasApprovedToAdmin::class,
        ],
        \App\Event\BookingWasApproved::class => [
            \App\Listeners\SendBookingWasApprovedToAdmin::class,
            \App\Listeners\SendBookingWasApprovedToUser::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
