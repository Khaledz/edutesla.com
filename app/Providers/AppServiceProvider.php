<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use App\Exercise;
// use App\Day;
// use App\AppSetting;
// use App\Booking;
// use App\Slider;
// use App\Observers\ExerciseObserver;
// use App\Observers\AppSettingObserver;
// use App\Observers\BookingObserver;
// use App\Observers\SliderObserver;
use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        // Exercise::observe(ExerciseObserver::class);
        // AppSetting::observe(AppSettingObserver::class);
        // Booking::observe(BookingObserver::class);
        // Slider::observe(SliderObserver::class);

        // $exercises = Cache::rememberForever('exercises', function () {
        //     return Exercise::get();
        // });

        // $days = Cache::rememberForever('days', function () {
        //     return Day::get();
        // });

        // $sliders = Cache::rememberForever('sliders', function () {
        //     return Slider::get();
        // });

        // $app_settings = Cache::rememberForever('app_settings', function () {
        //     return AppSetting::first();
        // });

        // \View::share('exercises', $exercises);
        // \View::share('days', $days);
        // \View::share('app_settings', $app_settings);
        // \View::share('sliders', $sliders);
    }
}
