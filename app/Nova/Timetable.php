<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\DateTime;
use Illuminate\Http\Request;
use Laraning\NovaTimeField\TimeField;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Epartment\NovaDependencyContainer\HasDependencies;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;

class Timetable extends Resource
{
    use HasDependencies;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Timetable';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Select::make('Type', 'type')->options([
                '1' => 'Every Day',
                '2' => 'Spesfic Date'
            ])->displayUsingLabels(),

            NovaDependencyContainer::make([
                BelongsTo::make('day', 'day', 'App\Nova\Day'),
            ])->dependsOn('type', '1'),

            NovaDependencyContainer::make([
                Date::make('Date'),
            ])->dependsOn('type', '2'),

            TimeField::make('From', 'time_from'),
            TimeField::make('To', 'time_to'),

            BelongsTo::make('Exercise'),
            Boolean::make('Published?', 'is_published'),
        ];
    }

    public function title()
    {
        return $this->exercise->name. ' من ' . \Carbon\Carbon::parse($this->time_from)->format('h:i') .' إلى '. \Carbon\Carbon::parse($this->time_to)->format('h:i');
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
