<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Timetable;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exercises = auth()->user()->exercises()->get();

        return view('booking.create', compact('exercises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Timetable $timetable)
    {
        $this->validate($request, [
            'id' => 'exists:timetables,id',
        ]);

        // Can't book the same timetable with the coming date.
        if(auth()->user()->hasBooked($timetable))
        {
            return back()->with('message', 'ﻻ يمكنك حجز هذا التمرين مره أخرى.');
        }

        // if the balance is less than exercise cost, show error
        if(auth()->user()->balance < $timetable->exercise->price)
        {
            return back()->with('message', 'رصيدك لا يكفي لحجز هذا التمرين، من فضلك قم بشحن رصيدك.');
        }

        $booking = new Booking();
        $booking->user_id = auth()->user()->id;
        $booking->timetable_id = $timetable->id;

        if(! is_null($timetable->date))
        {
            $booking->date = $timetable->date;
        }

        if(! is_null($timetable->day_id))
        {
            // get the coming date of day. ex: next monday
            $booking->date = Carbon::parse('This '. $timetable->day->en_day);
        }

        $booking->save();

        return back()->with('message', 'تم حجز هذا التمرين بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
