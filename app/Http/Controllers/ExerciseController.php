<?php

namespace App\Http\Controllers;

use App\Exercise;
use App\User;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exercise.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // store new exercise for user.
        $this->validate($request, ['exercise_id' => 'exists:exercises,id']);

        $exercise = Exercise::find($request->exercise_id);
        auth()->user()->exercises()->save($exercise);

        return back()->with('message', 'تم اﻹشتراك بنجاح في التمرين ' . $exercise->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function show(Exercise $exercise)
    {
        return view('exercise.show', compact('exercise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function edit(Exercise $exercise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exercise $exercise)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exercise $exercise, Request $request)
    {
        $this->validate($request, ['exercise_id' => 'exists:exercises,id']);

        $exercise = Exercise::find($request->exercise_id);
        auth()->user()->exercises()->detach($exercise->id);

        // remove all the bookings of this exercise
        $this->removeExerciseBookings(auth()->user(), $exercise);

        return back()->with('message', 'تم إلغاء اﻹشتراك بنجاح في التمرين ' . $exercise->name);
    }

    private function removeExerciseBookings(User $user, Exercise $exercise)
    {
        return $user->bookings()->delete();
    }
}
