<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => ['control_panel' => 'Dashboard',
	'setting' => 'Settings',
	'logout' => 'Logout',
	'payment' => 'Payments',
	'order' => 'Orders',
	'protofolio'=> 'Portfolio',
	'myprofile' => 'Profile',
	'email' => 'Email',
	'password' => 'Password',
	'forget_password' => 'Forgot your password?',
	'setting' => 'Settings',
	'main_information' => 'Main Information',
	'fname' => 'First Name',
	'lname' => 'Last Name',
	'country' => 'Country',
	'password_conf' => 'Password Confirmation',
	'password_empty' => 'Leave it blank if you do not want to change',
	'avatar' => 'Avatar',
	'avatar_empty' => 'Leave it blank if you do not want to change',
	'save_setting'=> 'Save Settings',
	'twitter'=> 'Twitter',
	'facebook' => 'Facebook',
	'instagram' => 'Instagram',
	'snapchat' => 'Snapchat',
	'about' => 'About me',
	'city' => 'City',
	'phone' => 'Phone',
	'nationality' => 'Nationality',
	'residence' => 'Residence Country',
	'gender' => 'Gender',
	'category' => 'Category',
	'extra_information' => 'Extra Information',
	'weight' => 'Weight (KG)',
	'height' => 'Height (CM)',
	'age' => 'Age',
	'eye_color' => 'Eye Color',
	'skin_color' => 'Skin Color',
	'hair_color' => 'Hair Color',
	'upload back and front picture of hands.' => 'upload back and front picture of hands.',
	'back_hand' => 'back hand picture.',
	'front_hand' => 'front hand picture.',
	'price_range' => 'Price Range',
	'price_type' => 'Price Type',
	'price' => 'Price',
	'upload_video' => 'Upload a video introducing yourself.',
	'paste_video_url' => 'Paste url of your video from youtube.com',
	'accents_you_speak' => 'What accents are you speaking?',
	'can_travel' => 'Can you travel outside your country?',
	'yes' => 'Yes',
	'no' => 'No',
	'rate' => 'Rate',
	'height_from' => 'Height From',
	'to' => 'To',
	'weight_from' => 'Weight From',
	'age_from' => 'Age From',
	'price_from' =>'Price From',
	'notcare' =>'No Matter',
	'search' => 'Search',
	'order_this_service' => 'Order',
	'study_university_in' => 'Studying university in',
	'study_language_in' => 'Studying language in',
	'study_in' => 'Studying in',
	'study_university' => 'Studying University',
	'study_language' => 'Studying Language',
	'about_edutesla' => 'About Edutesla',
	'speak_with_us' => 'Speak With Us',
	'speak_with_us_desc' => 'Would you like to get free consultation about your abroad study and getting University admission or visas? Apply now and we will contact you

',
	'success_partners' => 'Success Partners
',
	'select_destination_desc' => 'We offer you a variety of different destinations in the most famous countries in the world.
',
	'select_destination_language' => 'Select Destinations to Learn Languages',
	'select_destination' => 'Select Your Destination',
	'step1_desc' => 'The first step you will take is to choose your destination, and where you want to study.
',
	'step1' => 'Select Your Destination',
	'step2_desc' => 'Edutesla will take care of the rest of the processes from the first step of your request until getting your student visa.
',
	'step2' => 'Process your request
',
	'step3_desc' => 'Upon completion of your application you will be able to travel and start studying, wish you luck!
',
	'step3' => 'Wish you luck',

	'edutesla_for_education_desc_more' => '‏Every year we change the lives of thousands of students and families. We Let the students explore all the worldwide study opportunities from a single platform, and help them find the right study program that meets their needs, goals, and preferences. In order to succeed in this mission, we work with more than 400 universities, schools, and rehabilitation centers worldwide.',
	'edutesla_for_education' => 'Edutesla for Education Services',
	'edutesla_for_education_desc' => '‏Our company provide educational services in Turkey. We have three years of experience in guiding and advising our students that helped  thousands of families and students to move and live in Turkey.',
	'speak_now' => 'Speak With Us',
	'universitites' => 'Universities',
	'country_list' => 'Country List',
	'select_destination' => 'Select Destination',
	'location' => 'Locatoin',
	'about_edutesla_desc' => '‏Our company provide educational services in Turkey. We have three years of experience in guiding and advising our students that helped  thousands of families and students to move and live in Turkey.

‏Every year we change the lives of thousands of students and families. We Let the students explore all the worldwide study opportunities from a single platform, and help them find the right study program that meets their needs, goals, and preferences. In order to succeed in this mission, we work with more than 400 universities, schools, and rehabilitation centers worldwide.

‏In addition to students from Arab states we also assist students from many other countries such as Germany, USA, Canada, England, Poland, Malaysia, Austria, Russia and Ukraine.

‏Our branch offices are located in Istanbul, Egypt and Saudi Arabia.',
	'similar_language' => 'Similar Language Institutes',
	'language_logo' => 'Language Institute Logo',
	'apply_now' => 'Apply Now',
	'google_map' => 'Google Map',
	'quick_overview' => 'Quick Overview',
	'similar_university' => 'Similar Universities',
	'university_facts' => 'University in Numbers',
	'university_logo' => 'University Logo',
	'doctorat' => 'Doctorat',
	'master' => 'Master',
	'diploma' => 'Diploma',
	'bechalor' => 'Bachelor',
	'quick_links' => 'Quick Links',
	'footer_introduction' => '‏Our company provide educational services in Turkey. We have three years of experience in guiding and advising our students that helped  thousands of families and students to move and live in Turkey.
',
	'no_result' => 'There is no results at this moment.',
	'transaction' => 'Photo of Transaction',
'certificate' => 'Photo of heigh school or bechalor or master',
'avatar' => 'Personal Photo',
'passport' => 'PAssport Photo',
'official_document' => 'Official Documents',
'select_university' => 'Select University',
'major_to_study' => 'What the major you want to study?
',
'language_of_study' => 'Language of Study',
'degree_level' => 'Degree
',
'studying_information' => 'Studying Information',
'name' => 'Name',
'personal_information' => 'Personal Information',
'establishment_date' => 'Establishment Date',
'student_number' => 'Students Number',
'colleges_number' => 'Colleges Number',
'languages' => 'Language Institutes',
'send_message' => 'Send Message',
'message' => 'Your Message',
'subject' => 'Subject',
'contact_form' => 'Contact Form',
'address' => 'Address',],

];
