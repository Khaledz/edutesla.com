<?php

return [
    'create' => 'إضافة جديد',
    'edit' => 'تعديل',
    'home_page' => 'الصفحة الرئيسية',
    'user'=> 'المستخدمين',
    'category' => 'اﻷقسام',
    'order' => 'الطلبات',
    'show' => 'عرض',
    'login' => 'تسجيل الدخول',
	'register' => 'تسجيل حساب',
	'service_menu' => 'قائمة الخدمات',
	'about_us' => 'من نحن',
	'contact_us' => 'تواصل معنا',
	'copyright' => 'جميع الحقوق محفوظة',
	'social_account' => 'حسابات التواصل الأجتماعي',
	'terms' => 'شروط اﻹستخدام',
	'privacy' => 'سياسة الخصوصية',
	'professional_service' => 'خدمة إحترافية',
	'comprehensive' => 'خدمات شاملة',
	'understand_more' => 'نفهمك أكثر',
	'platforms' => 'نخدمك على جميع المنصات',
	'browse' => 'إستعرض',
	'professional_service_desc' => 'نقدم لك خدمة إحترافية بتوفير لك كل مايلزم لتبدأ مشروعك.',
    'comprehensive_desc' => 'نقدم خدمات متكاملة بجميع التخصصات.',
	'understand_more_desc' => 'ﻷننا نفهم إحتياجك، وفرنا لك جميع خصائص البحث الممكنة.',
	'platforms_desc' => 'تطبيقنا متوفر على جميع المنصات لنصل إليك، الويب والهاتف الذكي.',
	'profile' => 'حسابي الشخصي',
	'control_panel' => 'لوحة التحكم',
	'setting' => 'اﻹعدادات',
	'logout' => 'تسجيل الخروج',
	'payment' => 'الدفعات',
	'order' => 'الطلبات',
	'protofolio'=> 'اﻷعمال',
	'myprofile' => 'ملفي الشخصي',
	'email' => 'البريد اﻹلكتروني',
	'password' => 'كلمة المرور',
	'forget_password' => 'هل نسيت كلمة المرور؟',
	'setting' => 'اﻹعدادات',
	'main_information' => 'المعلومات اﻷساسية',
	'fname' => 'اﻹسم اﻷول',
	'lname' => 'اﻹسم اﻷخير',
	'country' => 'الدولة',
	'password_conf' => 'تأكيد كلمة المرور',
	'password_empty' => 'أترك الحقل فارغ إذا كنت ﻻ تريد تغييره.',
	'avatar' => 'الصورة الشخصية',
	'avatar_empty' => 'أترك الحقل فارغ إذا كنت ﻻ تريد تغييره.',
	'save_setting'=> 'حفظ اﻹعدادات',
	'twitter'=> 'تويتر',
	'facebook' => 'فيسبوك',
	'instagram' => 'إنستقرام',
	'snapchat' => 'سناب شات',
	'about' => 'مقدمة عني',
	'city' => 'المدينة',
	'phone' => 'رقم الجوال',
	'nationality' => 'الجنسية',
	'residence' => 'بلد اﻹقامة',
	'gender' => 'الجنس',
	'category' => 'القسم',
	'extra_information' => 'معلومات إضافية',
	'weight' => 'الوزن (KG)',
	'height' => 'الطول (CM)',
	'age' => 'العمر',
	'eye_color' => 'لون العينين',
	'skin_color' => 'لون البشرة',
	'hair_color' => 'لون الشعر',
	'upload back and front picture of hands.' => 'أرفع صورة ليدك من الجهة اﻷمامية والخلفية',
	'back_hand' => 'صورة يدك من الخلف.',
	'front_hand' => 'صورة يدك من اﻷمام.',
	'price_range' => 'النطاق السعري',
	'price_type' => 'نوع التسعير',
	'price' => 'السعر',
	'upload_video' => 'أرفع فيديو تعرف فيه عن نفسك.',
	'paste_video_url' => 'ألصف رابط الفيديو من موقع اليوتيوب.',
	'accents_you_speak' => 'أي اللهجات تجيد التحدث بها؟',
	'can_travel' => 'هل تستطيع السفر خارج البلد؟',
	'yes' => 'نعم',
	'no' => 'لا',
	'rate' => 'التقيم',
	'height_from' => 'الطول من',
	'to' => 'إلى',
	'weight_from' => 'الوزن من',
	'age_from' => 'العمر من',
	'price_from' =>'السعر من',
	'notcare' =>'لا يهم',
	'search' => 'بحث',
	'order_this_service' => 'اطلب',
	'study_university_in' => 'دراسة الجامعة في',
	'study_language_in' => 'دراسة اللغة في',
	'study_in' => 'الدراسة في',
	'study_university' => 'دراسة الجامعة',
	'study_language' => 'دراسة اللغة',
	'about_edutesla' => 'عن إيدوتسلا',
	'speak_with_us' => 'تحدث مع مستشارينا',
	'speak_with_us_desc' => 'هل لديك استفسارات؟ تحدث مع أحد مستشارينا لتحصل على المساعدة بخصوص التقديم في الجامعات أو المعاهد.',
	'success_partners' => 'شركاء النجاح',
	'select_destination_desc' => 'نقدم لكم باقة متنوعة من الوجهات المختلفة في أشهر بلدان العالم.',
	'select_destination_language' => 'إختر وجهتك لمعاهد اللغات',
	'select_destination' => 'إختر وجهتك',
	'step1_desc' => 'أول خطوة ستخطوها هي اختيار وجهتك والجهة التي تود الدراسة بها سواءاً كانت جامعة أو معهد لغة.',
	'step1' => 'أختر وجهتك',
	'step2_desc' => 'ستتكفل ايدوتسلا بباقي التفاصيل من أول استلامنا لطلبك حتى استخراج فيزتك الدراسية.

',
	'step2' => 'معالجة طلبك
',
	'step3_desc' => 'عند انتهاء طلبك ستكون قادر على السفر وبدء الدراسة، أمنياتنا لك بالتوفيق!

',
	'step3' => 'تمنياتنا لك بالتوفيق
',
	'edutesla_for_education_desc_more' => 'مؤسسة متعددة الجنسيات هدفها مساعدتك للدراسة في الخارج والحصول على التعليم المناسب لتكون أولى خطواتك نحو النجاح في حياتك العملية والتعليمية ، نحن وكلاء للجامعات في تركيا وعدة دول أخرى ولانتقاضى أي رسوم اضافية من الطالب ونقدم الاستشارات المجانية لكم في كل الأمور التعليمية ابتداء من اختيار التخصص والجامعة والمكان المناسب وحتى وصولك للدولة المقررة لبدء دراستك الجامعية بها ، نسعى دوماً للأفضل وسنكون كذلك بثقتكم وتشجيعكم .
',
	'edutesla_for_education' => 'ايدوتسلا للخدمات التعليمية',
	'edutesla_for_education_desc' => 'مؤسسة متعددة الجنسيات هدفها مساعدتك للدراسة في الخارج والحصول على التعليم المناسب لتكون أولى خطواتك نحو النجاح في حياتك العملية والتعليمية
',
	'speak_now' => 'تحدث معنا اﻵن',
	'universitites' => 'الجامعات',
	'country_list' => 'قائمةالدول',
	'select_destination' => 'إختر وجهتك',
	'location' => 'موقعنا',
	'about_edutesla_desc' => '					مؤسسة متعددة الجنسيات هدفها مساعدتك للدراسة في الخارج والحصول على التعليم المناسب لتكون أولى خطواتك نحو النجاح في حياتك العملية والتعليمية ، نحن وكلاء للجامعات في تركيا وعدة دول أخرى ولانتقاضى أي رسوم اضافية من الطالب ونقدم الاستشارات المجانية لكم في كل الأمور التعليمية ابتداء من اختيار التخصص والجامعة والمكان المناسب وحتى وصولك للدولة المقررة لبدء دراستك الجامعية بها ، نسعى دوماً للأفضل وسنكون كذلك بثقتكم وتشجيعكم .
نوفر لكم جميع المعلومات المتعلقة بالدراسة الجامعية في جميع أنحاء العالم وخدماتنا كالتالي:
1. القبول الجامعي (خاصة / حكومي) 
2. التقديم على المنح العالمية بشكل صحيح وكامل 100% ممايؤهلك للحصول على المنحة.
3. الاستشارات الأكاديمية.
4. دراسة الثانوية في تركيا.
6. القبول الجامعي في عدة بلدان أخرى :
تركيا ، روسيا ، جورجيا ، أوكرانيا ، ماليزيا ، الصين ، الهند ، مصر ، الأردن ، بيلاروسيا ، رومانيا ، المجر ، قبرص ، ألمانيا ، بولندا. 
7. إمكانية توفير منح تشمل خصم كامل على الرسوم الدراسية 100% أي الدراسة بشكل مجاني طوال فترة الجامعة.
8. جميع خدمات توفير القبولات الجامعية مجانية ولانتقاضى أي رسوم إضافية من الطالب. 
9. خدمات التقديم على المنح والجامعات الحكومية تتم برسوم رمزية وهي عبارة عن رسوم إدارية فقط. 
للتواصل معنا يرجى ارسال رسالة عبر البريد الإلكتروني متضمنة استفساراتك بالإضافة إلى رقم الواتساب الخاص بكم حيث يقوم فريقنا بالتواصل معكم هاتفياً. 
',
	'similar_language' => 'معاهد مشابهه',
	'language_logo' => 'شعار المعهد',
	'apply_now' => 'تقدم بطلبك اﻵن',
	'google_map' => 'خريطة قوقل',
	'quick_overview' => 'نظرة سريعة',
	'similar_university' => 'جامعات مشابهه',
	'university_facts' => 'أرقام عن الجامعة',
	'university_logo' => 'شعار الجامعة',
	'doctorat' => 'دكتوراه',
	'master' => 'ماجستير',
	'diploma' => 'دبلوم',
	'bechalor' => 'بكالريوس',
	'quick_links' => 'روابط سريعة',
	'footer_introduction' => 'مؤسسة متعددة الجنسيات هدفها مساعدتك للدراسة في الخارج والحصول على التعليم المناسب لتكون أولى خطواتك نحو النجاح في حياتك العملية والتعليمية ، نحن وكلاء للجامعات في تركيا وعدة دول أخرى ولانتقاضى أي رسوم اضافية من الطالب ونقدم الاستشارات المجانية لكم في كل الأمور التعليمية ابتداء من اختيار التخصص والجامعة والمكان المناسب وحتى وصولك للدولة المقررة لبدء دراستك الجامعية بها ، نسعى دوماً للأفضل وسنكون كذلك بثقتكم وتشجيعكم .

',
'transaction' => 'صورة كشف الدرجات',
'certificate' => 'صورة شهادة الثانوية أو البكالريوس أو الماجستير',
'avatar' => 'صورة شخصية',
'passport' => 'صورة الجواز',
'official_document' => 'المستندات الرسمية',
'select_university' => 'إختر الجامعة',
'major_to_study' => 'ما هو التخصص الذي تود دراسته؟
',
'language_of_study' => 'لغة الدراسة',
'degree_level' => 'الدرجة العلمية
',
'studying_information' => 'المعلومات الدراسية',
'name' => 'اﻹسم',
'personal_information' => 'المعلومات الشخصية',
'establishment_date' => 'تاريخ التأسيس',
'student_number' => 'عدد الطلاب',
'colleges_number' => 'عدد الكليات',
'languages' => 'معاهد اللغة',
'send_message' => 'أرسل رسالتك',
'message' => 'رسالتك',
'subject' => 'عنوان الرسالة',
'contact_form' => 'نموذج التواصل',
'address' => 'العنوان',
'post_categories' => 'مقالات ايدوتسلا',
'list_categories' => 'قائمة اﻷقسام',
'previous' => '« السابق',
'next' => 'اللاحق »',
'related_posts' => 'مقالات مشابهه',
'popular' => 'مقالات مشهورة',
'be_partner' => 'كن وكيلاً',
'company_name' => 'اسم الشركة',
'fill_partner_form' => 'قم بتعبئة النموذج التالي لتصبح وكيلاً لايدوتسلا في بلدك.',
'person_name' => 'اسم ممثل الشركة',
'send_now' => 'أرسل معلوماتك'


];