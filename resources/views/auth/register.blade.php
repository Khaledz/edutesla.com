@extends('layouts.app')
@section('title')
اﻹشتراك في النادي
@endsection
@section('content')
<div class="page-section wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-no-space"></div>
            <div class="col-sm-10 col-no-space">
                <div class="content-element1">
                    <div class="align-center">
                        <h3 class="section-title style-2">اﻹشتراك في النادي</h3>
                        <div class="icon-divider style-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 137.86 38" class="svg replaced-svg">
                                <defs>
                                    <style>
                                        .cls-1,
                                        .cls-2 {
                                            fill: none;
                                        }
                                        .cls-1,
                                        .cls-3 {
                                            stroke: #72695e;
                                            stroke-miterlimit: 10;
                                            opacity: 0.8;
                                        }
                                        .cls-3 {
                                            fill-opacity: 0.8;
                                            fill-rule: evenodd;
                                        }
                                    </style>
                                </defs>
                                <title>divider1</title>
                                <g id="Слой_2" data-name="Слой 2">
                                    <g id="Layer_1" data-name="Layer 1">
                                        <path class="cls-1" d="M88.05,27c-9,0-8.45,4-19,4s-10-4-19-4m25-7.25c0-5.32-2.63-9.3-6-13.75-3.37,4.45-6,8.43-6,13.75,0,3.81,2.07,7.25,6,7.25S75.05,23.56,75.05,19.75Zm-9-2.92C63.3,14,58,13,52.16,13c1.68,5.14,2.8,9.07,6.75,11.63C62,26.62,64.6,27,69.05,27,69.05,22.29,69,20,66,16.83Zm3,10.17c4.46,0,7.07-.38,10.15-2.37,3.95-2.56,5.06-6.48,6.75-11.62-5.89,0-11.14,1-13.85,3.83C69.1,20,69.05,22.29,69.05,27ZM65.62,11a18.06,18.06,0,0,0-6.56-4v6.58m20,0V7a18,18,0,0,0-6.56,4"></path>
                                        <rect class="cls-2" x="50.05" width="38" height="38"></rect>
                                        <path class="cls-3" d="M101.55,19h0s6.15-3,10-3c5.11,0,10,6,16,6a17.16,17.16,0,0,0,10-3h0a17.55,17.55,0,0,1-10,3c-6,0-10.83-6-16-6C107.66,16,101.55,19,101.55,19Z"></path>
                                        <path class="cls-3" d="M10.22,22c5.17,0,10-6,16-6a17.55,17.55,0,0,1,10,3h0a17.16,17.16,0,0,0-10-3c-6,0-10.89,6-16,6-3.85,0-10-3-10-3h0S6.33,22,10.22,22Z"></path>
                                    </g>
                                </g>
                            </svg></div>
                    </div>
                </div>
                {{ html()->form('post', 'register')->class('contact-form style-2')->open() }}
                @csrf
                <div class="contact-item">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="contact-item">
                                <label>اﻻسم اﻷول</label>
                                {{ html()->text('fname') }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="contact-item">
                                <label>اﻻسم اﻷخير</label>
                                {{ html()->text('lname') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-item">
                    <label>البريد اﻹلكتروني</label>
                    {{ html()->email('email') }}
                </div>
                <div class="contact-item">
                    <label>رقم الجوال</label>
                    {{ html()->text('phone') }}
                </div>
                <div class="contact-item">
                    <label>كلمة المرور</label>
                    {{ html()->password('password') }}
                </div>
                @if($exercises->count() > 0)
                <div class="contact-item">
                    <label>أختر التمارين المهتم بها؟ (تستطيع تغيير ذلك لاحقاً)</label>
                    <div class="input-wrapper">
                        @foreach($exercises as $key => $exercise)
                        <div class="form-check">
                            @if(isset(old('exercises')[$key]) && ! is_null(old('exercises')[$key]) && old('exercises')[$key] == $exercise->id)
                            <input type="checkbox" name="exercises[]" value="{{ $exercise->id }}" id="{{ $exercise->name }}" checked="" />
                            @else
                            <input type="checkbox" name="exercises[]" value="{{ $exercise->id }}" id="{{ $exercise->name }}" />
                            @endif
                            <label for="{{ $exercise->name }}">{{ $exercise->name }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
                <div class="contact-item align-center">
                    <button type="submit" class="btn btn-style-3">تسجيل عضوية جديدة</button>
                </div>
                {{ html()->form()->close() }}
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
</div>
@endsection