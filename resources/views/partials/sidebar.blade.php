<aside id="sidebar" class="col-lg-4 col-md-12">
    <div class="widget">
        <div class="widget-bg">
            <div class="team-holder">
                <div class="team-item flex-row justify-content-center">
                    <a href="#" class="member-photo">
                        <img src="http://brainwavescience.com/wp-content/uploads/2017/06/testi.png" alt="" width="104" height="104">
                    </a>
                    <div class="team-desc">
                        <div class="member-info align-center">
                            <h6 class="member-name">{{ $user->full_name }}</h6>
                            <h6 class="member-name">رقم العضوية: <b>#{{ $user->member_id }}</b></h6>
                            <h6 class="member-name">الرصيد: <b>{{ $user->balance }} ريال</b></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget">
        <div class="widget-bg">
            <div class="pbar-holder">
                <div class="pbar-wrap">
                    <!-- من اصل جميع التمارين -->
                    <div class="pbar-title">التمارين المسجلة <span>{{ auth()->user()->getExercisePercentage() }}%</span></div>
                    <div class="pbar">
                        <div class="pbar-inner" style="width: {{ auth()->user()->getExercisePercentage() }}%"></div>
                    </div>
                </div>
                <div class="pbar-wrap">
                    <!-- من اصل جميع الحجوزات -->
                    <div class="pbar-title">الحصص المؤكدة <span>{{ auth()->user()->getConfimredBookingPercentage() }}%</span></div>
                    <div class="pbar">
                        <div class="pbar-inner" style="width: {{ auth()->user()->getConfimredBookingPercentage() }}%"></div>
                    </div>
                </div>
                <div class="pbar-wrap">
                    <!-- حساب نسبة اﻻلغاء قبل الوقت -->
                    <div class="pbar-title">اﻹلتزام بالحضور <span>{{ auth()->user()->getCancellationPercentage() }}%</span></div>
                    <div class="pbar">
                        <div class="pbar-inner" style="width: {{ auth()->user()->getCancellationPercentage() }}%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>