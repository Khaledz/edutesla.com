<!-- START HEADER -->
<header class="header_wrap dark_skin main_menu_uppercase">
	<div class="top-header bg_gray">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <ul class="contact_detail border_list list_none text-center text-md-left">
                        <li>
                            <div class="custome_dropdown">
                                <select name="countries" class="custome_select">
                                    <option value='en' data-title="English">English</option>
                                    <option value='fn' data-title="العربية">العربية</option>
                                </select>
                            </div>
                        </li>
                        <li>
                          <a href="#">إشترك اﻵن</a>
                        </li>
                        <li>
                          <a href="#">تسجيل دخول</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-5">
                    <ul class="contact_detail border_list list_none text-center text-md-right">
                        <li><a href="#"><i class="ti-mobile"></i> <span>+123 456 7890</span></a></li>
                        <li><a href="#"><i class="ti-email"></i> <span>info@sdccards.com</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-lg"> 
            <a class="navbar-brand" href="index.html">
                <img class="logo_light" src="assets/images/logo_white.png" alt="logo" />
                <img class="logo_dark" src="assets/images/logo_dark.png" alt="logo" />
                <img class="logo_default" src="assets/images/logo_dark.png" alt="logo" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="ion-android-menu"></span> </button>
            <div class="collapse navbar-collapse navbar-rtl" id="navbarSupportedContent">
				<ul class="navbar-nav">
                    <li>
                        <a class="nav-link" href="/">الصفحة الرئيسية</a>
                    </li>
                    <li class="dropdown dropdown-mega-menu">
                        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">الماركات</a>
                        <div class="dropdown-menu">
                            <ul class="mega-menu d-lg-flex">
                            	<li class="mega-menu-col col-lg-3">
                                    <ul> 
                                        <li class="dropdown-header">Shop Pages</li> 
                                        <li><a class="dropdown-item nav-link nav_item" href="shop-list-view.html">Shop List View</a></li>
                                        <li><a class="dropdown-item nav-link nav_item" href="shop-grid-view.html">Shop Grid View</a></li>
                                        <li><a class="dropdown-item nav-link nav_item" href="shop-three-columns.html">shop 3 Column</a></li>
                                        <li><a class="dropdown-item nav-link nav_item" href="shop-four-columns.html">shop 4 Column</a></li>
                                    </ul>
                                </li>
                                <li class="mega-menu-col col-lg-3">
                                    <ul> 
                                        <li class="dropdown-header">Product Detail</li> 
                                        <li><a class="dropdown-item nav-link nav_item" href="product-detail.html">Product Default</a></li>
                                        <li><a class="dropdown-item nav-link nav_item" href="product-detail-left-sidebar.html">Product Left Sidebar</a></li>
                                        <li><a class="dropdown-item nav-link nav_item" href="product-detail-right-sidebar.html">Product Right Sidebar</a></li> 
                                    </ul>
                                </li>
                                <li class="mega-menu-col col-lg-3">
                                    <ul> 
                                        <li class="dropdown-header">Extra</li> 
                                        <li><a class="dropdown-item nav-link nav_item" href="cart.html">Cart</a></li>
                                        <li><a class="dropdown-item nav-link nav_item" href="checkout.html">Checkout</a></li>
                                        <li><a class="dropdown-item nav-link nav_item" href="wishlist.html">Wishlist</a></li> 
                                        <li><a class="dropdown-item nav-link nav_item" href="my-account.html">My Account</a></li> 
                                    </ul>
                                </li>
                                <li class="mega-menu-col col-lg-3">
                                    <div class="ads_banner">
                                    	<a href="#"><img src="assets/images/mega_menu_ads.jpg" alt="mega_menu_ads"/></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">كن شريكاً</a>
                        <div class="dropdown-menu">
                            <ul> 
                                <li><a class="dropdown-item nav-link nav_item" href="team.html">Our Team</a></li>
                                <li><a class="dropdown-item nav-link nav_item dropdown-toggler" href="#">Gallery</a>
                                	<div class="dropdown-menu">
                                        <ul> 
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-three-columns.html">Grid 3 Columns</a></li>
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-four-columns.html">Grid 4 Columns</a></li>
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-three-columns-wide.html">Grid 3 Columns Wide</a></li>
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-four-columns-wide.html">Grid 4 Columns Wide</a></li>
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-masonry-three-columns.html">Masonry 3 Columns</a></li>
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-masonry-four-columns.html">Masonry 4 Columns</a></li>
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-masonry-three-columns-wide.html">Masonry 3 Columns Wide</a></li>
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-masonry-four-columns-wide.html">Masonry 4 Columns Wide</a></li>
                                            <li><a class="dropdown-item nav-link nav_item" href="gallery-detail.html">Gallery Detail</a></li>
                                        </ul>
                                    </div>
                                </li>  
                                <li><a class="dropdown-item nav-link nav_item" href="faq.html">FAQ</a></li> 
                                <li><a class="dropdown-item nav-link nav_item" href="coming-soon.html">Coming Soon</a></li>
                                <li><a class="dropdown-item nav-link nav_item" href="404.html">404 Page</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a class="nav-link" href="/">إشترك اﻵن</a>
                    </li>
                </ul>
            </div>
            <ul class="navbar-nav attr-nav align-items-center">
                <li><a href="javascript:void(0);" class="nav-link search_trigger"><i class="ion-ios-search-strong"></i></a>
                    <div class="search-overlay">
                        <div class="search_wrap">
                            <form>
                                <div class="rounded_input">
                                	<input type="text" placeholder="Search" class="form-control" id="search_input">
                                </div>
                                <button type="submit" class="search_icon"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </li>
                <li class="dropdown cart_wrap">
                	<a class="nav-link" href="#" data-toggle="dropdown"><i class="ion-bag"></i><span class="cart_count">2</span></a>
                        <div class="cart_box dropdown-menu dropdown-menu-right">
                            <ul class="cart_list">
                                <li>
                                    <a href="#" class="item_remove"><i class="ion-close"></i></a>
                                    <a href="#"><img src="assets/images/cart_thamb1.jpg" alt="cart_thumb1">Fresh Organic Strawberry</a>
                                    <span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span>78.00</span></span>
                                </li>
                                <li>
                                    <a href="#" class="item_remove"><i class="ion-close"></i></a>
                                    <a href="#"><img src="assets/images/cart_thamb2.jpg" alt="cart_thumb2">Fresh Organic Grapes</a>
                                    <span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span>81.00</span></span>
                                </li>
                            </ul>
                        <div class="cart_footer">
                            <p class="cart_total">Total: <span class="cart_amount"> <span class="price_symbole">$</span>159.00</span></p>
                            <p class="cart_buttons"><a href="cart.html" class="btn btn-default btn-radius view-cart">View Cart</a><a href="checkout.html" class="btn btn-dark btn-radius checkout">Checkout</a></p>
                        </div>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</header>
<!-- END HEADER --> 
