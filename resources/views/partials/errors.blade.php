@if ($errors->any())
<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert"></button>
	@foreach ($errors->all() as $error)
		<p>{{ $error }}</p>
	@endforeach
</div>
@endif

@if(session('message'))
<div class="container">
<div class="alert alert-success">
<button type="button" class="close" data-dismiss="alert"></button>
{{ session('message') }}
</div>
</div>
@endif