<div class="breadcrumbs-wrap no-title">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">الصفحة الرئيسية</a></li>
            @if(! is_null(\Request::segment(1)) && is_null(\Request::segment(2)))
            <li><a href="{{ url('/' . \Request::segment(1)) }}">{{ __('breadcrumbs.' . \Request::segment(1))}}</a></li>
            @endif
            @if(! is_null(\Request::segment(1)) && ! is_null(\Request::segment(2)))
            <li><a href="#">{{ __('breadcrumbs.' . \Request::segment(1))}}</a></li>
            <li><a href="{{ url('/' . \Request::segment(1) .'/'.\Request::segment(2) ) }}">@yield('title')</a></li>
            @endif
        </ul>
    </div>
</div>