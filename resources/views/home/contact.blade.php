@extends('layouts.app')
@section('title')
تواصل معنا
@endsection
@section('content')
<div class="container page-section">
    <div class="row">
        <div class="col-md-3">
            <h4 class="title">New York</h4>
            <div class="our-info vr-type">
                <div class="info-item">
                    <i class="licon-map-marker"></i>
                    <span class="post">Postal Address</span>
                    <h6>8901 Marmora Road, Glasgow, D04 89GR.</h6>
                    <a href="https://www.google.com/maps/dir//2032+S+Elliott+Ave,+Aurora,+MO+65605/@36.9487043,-93.7878472,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x87cf4b1a194c90e1:0xba30bfe0c0a857c!2m2!1d-93.7178072!2d36.9487249" class="link-text">Get Direction</a>
                </div>
                <div class="info-item">
                    <i class="licon-telephone"></i>
                    <span class="post">Phone Number</span>
                    <h6 content="telephone=no">+1 800 559 6580</h6>
                </div>
                <div class="info-item">
                    <i class="licon-at-sign"></i>
                    <span class="post">Email Address</span>
                    <h6>info@companyname.com</h6>
                </div>
                <div class="info-item">
                    <i class="licon-clock3"></i>
                    <span class="post">Opening Hours</span>
                    <h6>Monday - Saturday: <br> 8am - 9pm</h6>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <form id="contact-form" class="contact-form style-2">
                <div class="contact-item">
                    <input type="text" name="cf-name" placeholder="Your Name (required)" required="">
                </div>
                <div class="contact-item">
                    <input type="email" name="cf-email" placeholder="Your Email (required)" required="">
                </div>
                <div class="contact-item">
                    <input type="url" name="cf-subject" placeholder="Subject">
                </div>
                <div class="contact-item">
                    <textarea rows="4" name="cf-message" placeholder="Your Message"></textarea>
                </div>
                <div class="contact-item align-center">
                    <button type="submit" class="btn btn-style-3" data-type="submit">Submit</button>
                </div>
                <div class="message-container"></div>
            </form>
        </div>
    </div>
</div>
<div class="map-section">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d29686.02269554767!2d39.180476959338364!3d21.556517700010726!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15c3d10044cd8edb%3A0x8f81370fb685f24b!2sAziz+Mall!5e0!3m2!1sen!2ssa!4v1558518894453!5m2!1sen!2ssa" width="600" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
@endsection